﻿using System;

public enum Language
{
    None,
    German,
    English
}
