﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

[Serializable]
public class TextSystem
{
    public static readonly string AppPath = Application.dataPath;
    public static readonly string Folder = "LanguageScripts";
    public static readonly string Prefix = ".lang";
    public static string GetPath(Language lang)
    {
        string path = Path.Combine(AppPath, Folder, lang.ToString() + Prefix);
        Directory.CreateDirectory(new FileInfo(path).Directory.FullName);
        return path;
    }

    public List<string> m_texts = new List<string> { "A;1", "B;2", "C;3" };

    public TextSystem(Language lang) =>
        LoadLanguage(lang);
    public TextSystem() { }

    public string this[string descriptor]
    {
        get { return GetValueOf(descriptor); }
        set { throw new Exception($"cannot set value of TextSystem. Modify Language file directly!"); }
    }

    private string GetValueOf(string descriptor)
    {
        return m_texts.
            Where(x => x.Split(';').ElementAt(0) == descriptor).
            Select(x => x.Split(';').ElementAt(1)).
            FirstOrDefault();
    }

    public void LoadLanguage(Language lang)
    {
        if (lang == Language.None)
            return;

        using (var stream = new StreamReader(GetPath(lang)))
        {
            var serializer = new XmlSerializer(GetType());
            m_texts = ((TextSystem) serializer.Deserialize(stream)).m_texts;
        }
    }

    public void SaveLanguage(Language lang)
    {
        if (lang == Language.None)
            return;

        using (var stream = new StreamWriter(GetPath(lang)))
        {
            var serializer = new XmlSerializer(GetType());
            serializer.Serialize(stream, this);
        }
    }
}
