﻿using System;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public Language language = Language.German;

    public void Start()
    {
        var sys = new TextSystem(Language.German);
        Debug.Log(sys["Language"]);
        Debug.Log(sys["PlayerName"]);
    }

    public void Update()
    {

    }
}
